![Screenshot](http://i.imgur.com/kqg8qxEh.png)

# Installation


Set up OpenFrameworks for your system, see: http://openframeworks.cc/download/ 
Put this folder in a subfolder under apps, such as YourOpenFrameworksFolder/apps/myapps/videowaves and compile it with your system-specific compiler.

If you are on Windows, you can find a package ready for download right [here](http://underhound.eu/tzer0/public_www/Programs/VideoWaves.zip) from my [website](http://underhound.eu).
Requires the following [package](https://www.microsoft.com/en-us/download/details.aspx?id=48145) (choose x86!).

# Instructions

* Requires a webcam.
* Shortcuts are outlined with paranthesis on each button. To hide/unhide the GUI, press h.
* The channels-parameter on the effects is a bitwise filter. It is mapped 1=red, 2=green, 4=blue. 7 means all channels, 5 means red and blue.
* Input modes are a mystery left to the user to discover. Hints:
	* Initialization factor only works for input mode 6.
	* Transmission factor and neighbourhood distance only works for input modes 5 and 6.
* Edge modes are in order: clamp, wrap, reflect.
* Your mouse-wheel can be used to tweak parameters accurately.

# Known issues

## Windows
* It is possible to compile using msys2-gcc, but I failed to share the binaries with anyone else as even with the right DLLs it simply doesn't work. The binaries provided for Windows have thus been compiled with MSVC2015.
* Compiling using msys2-gcc fails without a fix to headers installed by pacman in msys2. See [this post](https://forum.openframeworks.cc/t/of-0-9-3-error-compiling-of-on-windows/23611/9).

## Mac
* I don't own a mac, so I haven't tried it. Let me know how it goes if you do set this up.

# Cool things you can do

See this [gallery](http://imgur.com/a/uAQkd).

* Point your webcamera at the screen and enable xor with value 255. Mess around with the channels for the XOR while doing this.
* Input mode 6 with neighbourhood distance 1 or 0, high transmission and init factors.
* Input mode 6, transmission at 0.7, initialization at 0.88, add a LIMIT-effect, set lower: -175 or lower (mess around with this), upper to 160 and enable modulate in range.
* Try to become [Andy Warhol](https://www.google.com/search?q=andy+warhol+art&tbm=isch).
