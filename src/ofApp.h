#pragma once

#include "ofMain.h"
#include <vector>
#include "ofxGui.h"
#include "ofxOscParameterSync.h"

enum FX_TYPE {
	FX_XOR,
	FX_AND,
	FX_OR,
	FX_ROLL,
	FX_MODULATE,
	FX_MULT,
	FX_MOD,
	FX_LIMIT,
	FX_MAX,
	FX_NONE
};

enum INPUT_MODE {
	INPUT_STANDARD = 0,
	INPUT_LAST_XOR,
	INPUT_LAST_AND,
	INPUT_LAST_OR,
	INPUT_LAST_ABS_DIFF,
	INPUT_SOME_GHOSTS,
	INPUT_HEAT_GHOSTS,
	INPUT_MAX
};

enum EDGE_MODE {
	EDGE_CLAMP,
	EDGE_WRAP,
	EDGE_REFLECT,
	EDGE_MAX
};

class FX {
public:
	FX() : channels("Channels", 7, 0, 7) {  }
	const FX_TYPE getType() const { return type; }
	std::string name;
	std::vector<ofParameter<int>> int_params;
	std::vector<ofParameter<float>> float_params;
	ofParameter<int> channels;
	FX_TYPE type = FX_NONE;
};

class FXXor : public FX {
public:
	FXXor() {
		name = "XOR";
		type = FX_XOR;
		int_params.push_back(ofParameter<int>("Value", 128, 0, 255));
	}

	~FXXor() {}
};

class FXAnd : public FX {
public:
	FXAnd() {
		name = "AND";
		type = FX_AND;
		int_params.push_back(ofParameter<int>("Value", 128, 0, 255));
	}

	~FXAnd() {}
};

class FXOr : public FX {
public:
	FXOr() {
		name = "OR";
		type = FX_OR;
		int_params.push_back(ofParameter<int>("Value", 128, 0, 255));
	}

	~FXOr() {}
};

class FXRoll : public FX {
public:
	FXRoll() {
		name = "ROLL";
		type = FX_ROLL;
		int_params.push_back(ofParameter<int>("Value", 4, 0, 7));
	}

	~FXRoll() {}
};

class FXModulate: public FX {
public:
	FXModulate() {
		name = "MODULATE";
		type = FX_MODULATE;
		int_params.push_back(ofParameter<int>("Value", 0, -512, 512));
	}

	~FXModulate() {}
};

class FXMultiply: public FX {
public:
	FXMultiply() {
		name = "MULTIPLY";
		type = FX_MULT;
		float_params.push_back(ofParameter<float>("Value", 1, -100, 100));
	}

	~FXMultiply() {}
};

class FXMod: public FX {
public:
	FXMod() {
		name = "MOD";
		type = FX_MOD;
		int_params.push_back(ofParameter<int>("Value", 64, 1, 512));
	}

	~FXMod() {}
};

class FXLimit: public FX {
public:
	FXLimit() {
		name = "LIMIT";
		type = FX_LIMIT;
		int_params.push_back(ofParameter<int>("Lower", 64, -512, 512));
		int_params.push_back(ofParameter<int>("Upper", 192, -512, 512));
		int_params.push_back(ofParameter<int>("Modulate in range", 0, 0, 1));
	}

	~FXLimit() {}
};

class FxButtonLinker;

class ofApp : public ofBaseApp{

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	void initGui();
	void deleteFx();
	void swapFx();
	void addFx(FX_TYPE type);
	void addXor() { addFx(FX_XOR); }
	void addOr() { addFx(FX_OR); }
	void addAnd() { addFx(FX_AND); }
	void addRoll() { addFx(FX_ROLL); }
	void addModulate() { addFx(FX_MODULATE); }
	void addMult() { addFx(FX_MULT); }
	void addMod() { addFx(FX_MOD); }
	void addLimit() { addFx(FX_LIMIT); }

	ofVideoGrabber m_vidGrabber;
	ofPixels m_outVideo;
	ofPixels_<int> m_prevPixels, m_prevOutVideo;
	ofPixels_<int> m_procBuf;
	ofxButton m_deleteButton, m_swapButton;
	ofxIntSlider m_v1, m_v2, m_inputMode, m_neighDistance, m_edgeMode;
	ofxFloatSlider m_initFactor, m_transFactor;
	ofTexture m_videoTexture;
	int m_camWidth;
	int m_camHeight;
	int m_h, m_w;
	bool m_firstFrame, m_hide;
	ofxPanel m_gui, m_effectsGui;
	ofxPanel m_inputParameters;
	std::vector<FX> m_fx;
	std::vector<ofxButton> m_fxAddButtons;

	inline unsigned char charLimit(const int v, const int lower = 0, const int upper = 255) {
		return std::max((unsigned char) (lower%256), std::min((unsigned char) (upper%256), (unsigned char) (v%256)));
	}

	inline int limit(const int v, const int lower = 0, const int upper = 255) {
		return std::max(lower, std::min(upper, v));
	}

	inline int getPos(int x, int y, const int off) {
		switch (m_edgeMode) {
		case EDGE_CLAMP:
			x = limit(x, 0, m_camWidth-1);
			y = limit(y, 0, m_camHeight-1);
			break;
		case EDGE_REFLECT:
			x = std::abs(x) - ((x >= m_camWidth) * (x % (m_camWidth-1)));
			y = std::abs(y) - ((y >= m_camHeight) * (y % (m_camHeight-1)));
			break;
		case EDGE_WRAP:
			x = (x + m_camWidth) % m_camWidth;
			y = (y + m_camHeight) % m_camHeight;
			break;
		default:
			break;
		}

		return x*3 + y*m_camWidth*3 + off;
	}
	~ofApp() {
	}
};
