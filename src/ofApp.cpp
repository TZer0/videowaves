#include "ofApp.h"


void ofApp::setup(){
	ofSetWindowTitle("VideoWaves");
	windowResized(1024, 768);
	m_camWidth = 1024/2;  // try to grab at this size.
	m_camHeight = 768/2;
	m_firstFrame = true;

	//we can now get back a list of devices.
	vector<ofVideoDevice> devices = m_vidGrabber.listDevices();

	for(int i = 0; i < devices.size(); i++){
		if(devices[i].bAvailable){
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
		}else{
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
		}
	}

	m_vidGrabber.setDeviceID(0);
	m_vidGrabber.setDesiredFrameRate(60);
	m_vidGrabber.initGrabber(m_camWidth, m_camHeight);

	m_outVideo.allocate(m_camWidth, m_camHeight, OF_PIXELS_RGB);
	m_prevOutVideo.allocate(m_camWidth, m_camHeight, OF_PIXELS_RGB);
	m_prevPixels.allocate(m_camWidth, m_camHeight, OF_PIXELS_RGB);
	m_procBuf.allocate(m_camWidth, m_camHeight, OF_PIXELS_RGB);
	m_videoTexture.allocate(m_outVideo);
	ofSetVerticalSync(true);
	ofEnableAlphaBlending();

	// Set up FX adding buttons
	for (int i = 0; i < FX_MAX; i++) {
		m_fxAddButtons.push_back(ofxButton());
	}
	m_fxAddButtons[0].setup("Add (X)OR effect");
	m_fxAddButtons[0].addListener(this, &ofApp::addXor);
	m_fxAddButtons[1].setup("Add (O)R effect");
	m_fxAddButtons[1].addListener(this, &ofApp::addOr);
	m_fxAddButtons[2].setup("Add (A)ND effect");
	m_fxAddButtons[2].addListener(this, &ofApp::addAnd);
	m_fxAddButtons[3].setup("Add (R)OLL effect");
	m_fxAddButtons[3].addListener(this, &ofApp::addRoll);
	m_fxAddButtons[4].setup("Add (M)ODULATE effect");
	m_fxAddButtons[4].addListener(this, &ofApp::addModulate);
	m_fxAddButtons[5].setup("Add M(U)LT effect");
	m_fxAddButtons[5].addListener(this, &ofApp::addMult);
	m_fxAddButtons[6].setup("Add MO(D) effect");
	m_fxAddButtons[6].addListener(this, &ofApp::addMod);
	m_fxAddButtons[7].setup("Add (L)IMIT effect");
	m_fxAddButtons[7].addListener(this, &ofApp::addLimit);

	// Button init
	m_deleteButton.setup("Delete V1");
	m_swapButton.setup("Swap V1 and V2");
	m_deleteButton.addListener(this, &ofApp::deleteFx);
	m_swapButton.addListener(this, &ofApp::swapFx);

	// Slider init
	m_v1.setup("V1", 0, 0, std::max(0, (int) m_fx.size()-1));
	m_v2.setup("V2", 0, 0, std::max(0, (int) m_fx.size()-1));
	m_inputMode.setup("Input mode", 0, 0, INPUT_MAX-1);
	m_neighDistance.setup("Neighbourhood distance", 8, 0, 32);
	m_initFactor.setup("Initialization", 1.0, 0, 10);
	m_transFactor.setup("Transmission", 0.22, 0, 2.0);
	m_edgeMode.setup("Edge mode", 0, 0, EDGE_MAX-1);
	m_hide = false;

	initGui();
}


void ofApp::addFx(FX_TYPE type) {
	switch (type) {
	case FX_XOR:
		m_fx.push_back(FXXor());
		break;
	case FX_AND:
		m_fx.push_back(FXAnd());
		break;
	case FX_OR:
		m_fx.push_back(FXOr());
		break;
	case FX_ROLL:
		m_fx.push_back(FXRoll());
		break;
	case FX_MODULATE:
		m_fx.push_back(FXModulate());
		break;
	case FX_MULT:
		m_fx.push_back(FXMultiply());
		break;
	case FX_MOD:
		m_fx.push_back(FXMod());
		break;
	case FX_LIMIT:
		m_fx.push_back(FXLimit());
		break;
	}
	initGui();
}

void ofApp::swapFx() {
	if (m_v1 < m_fx.size() && m_v2 < m_fx.size() && m_v1 != m_v2) {
		std::swap(m_fx[m_v1], m_fx[m_v2]);
	}
	initGui();
}

void ofApp::deleteFx() {
	if (m_v1< m_fx.size()) {
		m_fx.erase(m_fx.begin() + m_v1);
		m_v1 = std::max(0, std::min((int) m_v1, (int) m_fx.size() - 1));
		m_v2 = std::max(0, std::min((int) m_v2, (int) m_fx.size() - 1));
		initGui();
	}
}

void ofApp::initGui() {
	m_gui.clear();
	m_gui.setup();
	m_effectsGui.clear();
	m_effectsGui.setup();
	ofParameterGroup allParameters;
	allParameters.setName("Effect-chain");
	for (size_t i = 0; i < m_fx.size(); i++) {
		auto fx = m_fx[i];
		ofParameterGroup parameters;
		parameters.setName(std::to_string(i) + " - " + fx.name);
		parameters.add(fx.channels);
		for (auto &param : fx.int_params) {
			parameters.add(param);
		}
		for (auto &param : fx.float_params) {
			parameters.add(param);
		}
		allParameters.add(parameters);
	}
	m_effectsGui.add(allParameters);
	m_v1.setMax(std::max(0, (int) m_fx.size()-1));
	m_v2.setMax(std::max(0, (int) m_fx.size()-1));
	m_effectsGui.add(&m_v1);
	m_effectsGui.add(&m_v2);
	m_effectsGui.add(&m_deleteButton);
	m_effectsGui.add(&m_swapButton);

	m_gui.add(&m_inputMode);
	m_gui.add(&m_neighDistance);
	m_gui.add(&m_initFactor);
	m_gui.add(&m_transFactor);
	m_gui.add(&m_edgeMode);
	for (auto &fxAddButton : m_fxAddButtons) {
		m_gui.add(&fxAddButton);
	}
}

//--------------------------------------------------------------
void ofApp::update(){
	ofBackground(100, 100, 100);
	m_vidGrabber.update();

	if(m_vidGrabber.isFrameNew()){
		ofPixels & pixels = m_vidGrabber.getPixels();
		for(int y = 0; y < m_camHeight; y++){
			for(int x = 0; x < m_camWidth; x++){
				for (int off = 0; off < 3; off++) {
					int c = getPos(x, y, off);
					if (m_firstFrame) {
						m_outVideo[c] = 0;
						m_prevOutVideo[c] = 0;
						m_procBuf[c] = 0;
					}
					unsigned char out;
					switch (m_inputMode) {
					case INPUT_STANDARD:
						out = pixels[c];
						break;
					case INPUT_LAST_XOR:
						out = m_prevPixels[c] ^ pixels[c];
						break;
					case INPUT_LAST_OR:
						out = m_prevPixels[c] | pixels[c];
						break;
					case INPUT_LAST_AND:
						out = m_prevPixels[c] & pixels[c];
						break;
					case INPUT_LAST_ABS_DIFF:
						out = limit(std::abs(((int)m_prevPixels[c])-pixels[c]));
						break;
					case INPUT_SOME_GHOSTS:
					{
						int up = getPos(x, y-m_neighDistance, off);
						int down = getPos(x, y+m_neighDistance, off);
						int left = getPos(x+m_neighDistance, y, off);
						int right = getPos(x-m_neighDistance, y, off);
						out = limit(std::abs((int) (((int)m_prevPixels[c])-pixels[c] +
							m_transFactor/4 * (m_prevOutVideo[up] + m_prevOutVideo[down] + m_prevOutVideo[left] + m_prevOutVideo[right]))));
					}
						break;
					case INPUT_HEAT_GHOSTS:
					{
						int up = getPos(x, y-m_neighDistance, off);
						int down = getPos(x, y+m_neighDistance, off);
						int left = getPos(x+m_neighDistance, y, off);
						int right = getPos(x-m_neighDistance, y, off);
						out = limit((int) (std::abs((int) (m_initFactor*
							(((float)m_prevPixels[up])-pixels[up] + m_prevPixels[down] - pixels[down] + m_prevPixels[left]
									- pixels[left] + m_prevPixels[right] - pixels[right] + m_prevPixels[c] - pixels[c] ))))+
							m_transFactor/4 *(m_prevOutVideo[up] + m_prevOutVideo[down] + m_prevOutVideo[left] + m_prevOutVideo[right]));
					}
						break;
					}
					m_procBuf[c] = out;

				}
			}
		}

		for (const auto &fx : m_fx) {
			for (int off = 0; off < 3; off++) {
				if ((fx.channels & (1 << off)) == 0) {
					continue;
				}
				for(int y = 0; y < m_camHeight; y++){
					for(int x = 0; x < m_camWidth; x++){
						int c = getPos(x, y, off);
						switch (fx.getType()) {
						case FX_XOR:
							{
								m_procBuf[c] ^= fx.int_params[0].get();
							}
							break;
						case FX_AND:
							{
								m_procBuf[c] &= fx.int_params[0].get();
							}
							break;
						case FX_OR:
							{
								m_procBuf[c] |= fx.int_params[0].get();
							}
							break;
						case FX_ROLL:
							{
								unsigned char r = fx.int_params[0].get();
								unsigned char out = charLimit(m_procBuf[c]);
								m_procBuf[c] = (out << r)%256 | (out >> (8-r));
							}
							break;
						case FX_MODULATE:
							{
								m_procBuf[c] += fx.int_params[0].get();
							}
							break;
						case FX_MULT:
							{
								m_procBuf[c] *= fx.float_params[0].get();
							}
							break;
						case FX_MOD:
							{
								m_procBuf[c] %= fx.int_params[0].get();
							}
							break;
						case FX_LIMIT:
							{
								int l = fx.int_params[0].get();
								int u = fx.int_params[1].get();
								int mod_in_range = fx.int_params[2].get();
								m_procBuf[c] = limit(m_procBuf[c], l, u) + mod_in_range *((m_procBuf[c] - l) % limit((u - l), 1, 512));
							}
							break;
						default:
							break;
						}
					}
				}
			}
		}
		for(int i = 0; i < pixels.size(); i++){
			m_prevPixels[i] = pixels[i];
			m_prevOutVideo[i] = m_procBuf[i];
			m_outVideo[i] = limit(m_procBuf[i]);
		}
		m_videoTexture.loadData(m_outVideo);
		m_firstFrame = false;
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetHexColor(0xffffff);
	m_videoTexture.draw(0, 0, m_w, m_h);
	if (!m_hide) {
		m_gui.setPosition(0,0);
		m_effectsGui.setPosition(m_w-m_effectsGui.getWidth(),0);
		m_gui.draw();
		m_effectsGui.draw();
	}
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	// in fullscreen mode, on a pc at least, the
	// first time video settings the come up
	// they come up *under* the fullscreen window
	// use alt-tab to navigate to the settings
	// window. we are working on a fix for this...

	// Video settings no longer works in 10.7
	// You'll need to compile with the 10.6 SDK for this
	// For Xcode 4.4 and greater, see this forum post on instructions on installing the SDK
	// http://forum.openframeworks.cc/index.php?topic=10343
	if(key == 's' || key == 'S'){
		m_vidGrabber.videoSettings();
	}
	if(key == 'x' || key == 'X'){
		addXor();
	}
	if(key == 'a' || key == 'A'){
		addAnd();
	}
	if(key == 'o' || key == 'O'){
		addOr();
	}
	if(key == 'r' || key == 'R'){
		addRoll();
	}
	if(key == 'h' || key == 'H'){
		m_hide = !m_hide;
	}
	if(key == 'm' || key == 'M'){
		addModulate();
	}
	if(key == 'u' || key == 'U'){
		addMult();
	}
	if(key == 'd' || key == 'D'){
		addMod();
	}
	if(key == 'l' || key == 'L'){
		addLimit();
	}
	initGui();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	m_w = w; m_h = h;
	ofSetWindowShape(m_w, m_h);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
